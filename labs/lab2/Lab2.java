
//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #2
//Date: 2 September 2015
//
//Purpose: To convert a temperature from degrees fahrenheit to degrees celcius.
//****************************************
import java.util.Date; // needed for printing today's date

public class Lab2
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kimie Ellis\nLab # \n" + new Date() + "\n");

		//The formula to convert something from Fahrenheit to Celcius is as follows: degCel = (degFahr - 32) * 5/9

		//Variable dictionary:
		int degreeFahr = 350; //typical oven setting in degrees Fahrenheit
		int variable1 = 32; //number we will subtract from degree Fahrenheit
		double numerator = 5.0; //numerator of the fraction we will multiply
		double denominator = 9.0; //denominator of the fraction we will multiply
		int stepOne; //first step in the calculation
		double variable2; // 5/9
		double degreeCel; //formula to calculate degrees celcius

		//Compute values:
		stepOne = degreeFahr - variable1;
		variable2 = numerator / denominator;
		degreeCel = stepOne * variable2;

		System.out.println("Cookies bake at: " + degreeFahr + " degrees Fahrenheit.");
		System.out.println("To convert to celcius, we first subtract " + variable1 + " from " + degreeFahr + " degrees Fahrenheit, which equals " + stepOne + ".");
		System.out.println("Then we compute the second variable by calculating the fraction " + numerator + " over " + denominator + " to get " + variable2 + ".");
		System.out.println("Next we multiply " + stepOne + " by " + variable2 + ".");
		System.out.println("Thus, the final temperature is " + degreeCel + " degrees celcius.");

//variable2 is coming out as zero, is the division sign wrong?
	}
}
  
