
//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #3
//Date: 9 September 2015
//
//Purpose: Create a program that will calculate tip, total bill, and each person's share for a restaurant bill.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed to utilize the scanner function
import java.text.NumberFormat; //allows for formatting numbers into monetary value

public class Lab3
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kimie Ellis\nLab #3 \n" + new Date() + "\n");

		Scanner scan = new Scanner(System.in); //introduces the scanning function to the program
		NumberFormat fmt = NumberFormat.getCurrencyInstance(); //introduces the formatting function

		//Variable dictionary:
		double tipDollar; //the tip owed, in dollars
		double tipPercent; //the percentage of the bill the user would like to tip
		double bill; //the original bill amount, in dollars
		double billTotal; //the value of the bill and the tip together, in dollars
		double billSplit; //the amount each person will pay when splitting the bill, in dollars
		int num_people; //the number of people splitting the bill
		String name; //the name of the person using the program


		System.out.println("Please enter your name: ");
		name = scan.nextLine(); //waits for input within the terminal window, utilizing the scanning function
		System.out.println("Welcome to the tip calculator, " + name + "!");
	
		System.out.println("Please enter the amount of your bill: ");
		bill = scan.nextDouble();

		System.out.println("Please enter the percentage, out of 100, that you would like to tip: ");
		tipPercent = scan.nextDouble();

		tipDollar = tipPercent/100 * bill; //calculates the tip using the original bill a preferred tip percentage
		billTotal = bill + tipDollar; //the original bill plus the calculated tip gives the bill total

		System.out.println("Your original bill was " + fmt.format(bill) + "."); //the following amounts will be displayed in currency format
		System.out.println("Your tip amount is " + fmt.format(tipDollar) + ".");
		System.out.println("Your total bill is " + fmt.format(billTotal) + ".");
		
		System.out.println("How many people will be splitting the bill?");
		num_people = scan.nextInt();

		billSplit = billTotal/num_people; //amount each person splitting the bill will pay

		System.out.println("Each person should pay " + fmt.format(billSplit) + ".");
		System.out.println("Thank you for using our tip calculating service, " + name + ". Have a nice day!");
	}
}
  
