//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #7
//Date: 7 October 2015
//
//Purpose: Debug an existing code, practice using the Math class, and practice calling methods from another Java class.
//****************************************

public class GeometricCalculator {

    public static double calculateSphereVolume(double radius) {
        double volume;
        volume = (4/3.0) * (Math.PI) * radius * radius * radius; //fixed bug: bugged version said 3/4, should be 4/3
	//must change 3 to 3.0 so java reads 4/3 as a double instead of an integer
        return volume; //produces volume output for CommandLineGeometer
    }

    public static double calculateSphereSurfaceArea(double radius) {
	double surfaceArea;
	surfaceArea = 4 * (Math.PI) * radius * radius; //formula to find surface area for a sphere
	return surfaceArea; //produces surface area output for CommandLineGeometer

    }

    public static double calculateTriangleArea(double a, double b, double c) {
        double area;
	double perim, s; //declares perim and s
	double first, second, third; 

	perim = (a + b + c); //defines the perimeter of the triangle
	s = (1/2.0)*perim; //define s as half the perimeter
	//define the terms used in the formula:
	first = (s-a); 
	second = (s-b);
	third = (s-c);

        area = Math.sqrt(s*first*second*third); //bugged equation: area = a * a+b * c, should be: area = sqrt{s(s-a)(s-b)(s-c)}
        return area; //produces area output for CommandLineGeometer

    }

    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * radius * radius * height; //bugged code had pi(r^3)h, it should be pi(r^2)h
        return volume; //produces volume output for CommandLineGeometer

    }

    public static double calculateCylinderSurfaceArea(double radius, double height) {
	double surfaceArea;
	surfaceArea = 2 * (Math.PI) * radius * height; //formula for surface area of a cylinder
	return surfaceArea; //produces surface area output for CommandLineGeometer

    }
}
