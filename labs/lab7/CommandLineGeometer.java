//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #7
//Date: 7 October 2015
//
//Purpose: Debug an existing code, practice using the Math class, and practice calling methods from another Java class.
//****************************************
import java.util.Date;
import java.util.Scanner; //imports the scanner class
import java.text.DecimalFormat; //imports the decimal formatting class

public class CommandLineGeometer {

    private enum GeometricShape {sphere, triangle, cylinder}; //defines the different shapes that will be called upon

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
	DecimalFormat fmt = new DecimalFormat("0.####"); //formats numbers to the fourth decimal place

        GeometricShape shape = GeometricShape.sphere; //states that we are starting with the sphere
        double radius;
        double height;
        int x;
        int y;
        int z;

        System.out.println("Kimie Ellis " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble(); //retrieves sphere radius from user
        System.out.println(); //creates an empty line in the output for easier reading

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius); //calls upon the sphereVolume variable in the GeometricCalculator
        System.out.println("The volume is equal to " + fmt.format(sphereVolume)); //prints the calculated volume
        System.out.println(); 

	System.out.println("Calculating the surface aread of a " + shape + " with radius equal to " + radius);
	double sphereSurfaceArea = GeometricCalculator.calculateSphereSurfaceArea(radius); //calls upon the sphereSurfaceArea variable in the GeometricCalculator
	System.out.println("The surface area is equal to " + fmt.format(sphereSurfaceArea)); //prints the calculated surface area
	System.out.println();

       shape = GeometricShape.triangle; //switches the shape to the triangle

	//retrieves the side lengths from the user:
        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("Calculating the area of a " + shape + " with side lengths " + x + ", " + y + ", and " + z);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z); //bugged version said (x,y,x), should be (x,y,z)
        System.out.println("The area is equal to " + fmt.format(triangleArea)); //prints the calculated area
        System.out.println(); 

        shape = GeometricShape.cylinder; //switches the shape to the cylinder
	
	//retrieve the radius and height information from the user:
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius + " and height equal to " + height);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height); //calls upon the cylinderVolume variable in GeometricCalculator
        System.out.println("The volume is equal to " + fmt.format(cylinderVolume)); //prints the calculated volume
        System.out.println(); 

	System.out.println("Calculating the surface area of a " + shape + " with radius equal to " + radius + " and height equal to " + height);
	double cylinderSurfaceArea = GeometricCalculator.calculateCylinderSurfaceArea(radius, height); //calls upon the cylinderSurfaceArea variable in GeometricCalculator
	System.out.println("The surface area is equal to " + fmt.format(cylinderSurfaceArea)); //prints the calculated surface area
	System.out.println();

	
    }
}
