//****************************************
// Kimie Ellis
// CMPSC 111
// 26 August 2015
// Lab 1 Part 2
//****************************************

import java.util.Date;

public class Lab1Part2
{
	public static void main(String[] args)
	{
		System.out.println("Kimie Ellis " + new Date());
		System.out.println("Lab 1 Part 2");

		//--------------------------------
		//Prints words of wisdom
		//--------------------------------

		System.out.println("My favorite quote:");
		System.out.println("If you walk the footsteps of a stranger, you'll learn things you never knew you never knew.");
		System.out.println("Pocahontus, Colors of the Wind");
	}
}
