//****************************************
//Kimie Ellis
//CMPSC 111
//26 August 2015
//Lab 1
//
//Listing 1.1 from Lewis & Loftus, slightly modified.
//Demonstrates the basic structure of a Java application.
//****************************************

import java.util.Date;

public class Lab1
{
	public static void main(String[] args)
	{
		System.out.println("Kimie Ellis " + new Date());
		System.out.println("Lab 1");

		//------------------------------
		//Prints words of wisdom
		//------------------------------

		System.out.println("Advice from James Gosling, creator of Java:");
		System.out.println("Don't be intimidated--give it a try!");
	}
}

