//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #8
//Date: 21 October 2015
//
//Purpose: Create a program that checks whether the inputs from a 4x4 Sudoku game are correct.
//****************************************
import java.util.Date; // needed for printing today's date

public class SudokuCheckerMain
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kimie Ellis\nLab #8 \n" + new Date() + "\n");

        SudokuChecker checker = new SudokuChecker();

        //welcome message:
        System.out.println("Welcome to the Sudoku Checker! \n");
        System.out.println("This program checks simple and small 4x4 Sudoku grids for correctness.");
        System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once. \n");


        checker.getGrid();
        checker.checkGrid();
	}
}

