//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Lab #8
//Date: 21 October 2015
//
//Purpose: Create a program that checks whether the inputs from a 4x4 Sudoku game are correct.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //import scanner function

public class SudokuChecker
{

    Scanner scan = new Scanner(System.in);

        //declare private data members
        private int w1, w2, w3, w4; //integers input for row 1
        private int x1, x2, x3, x4; //row 2
        private int y1, y2, y3, y4; //row 3
        private int z1, z2, z3, z4; //row 4

        //define the constructor
        public SudokuChecker (){
          //initialize each private integer to zero to show that the user has not yet assigned a value
            w1 = 0;
            w2 = 0;
            w3 = 0;
            w4 = 0;
            x1 = 0;
            x2 = 0;
            x3 = 0;
            x4 = 0;
            y1 = 0;
            y2 = 0;
            y3 = 0;
            y4 = 0;
            z1 = 0;
            z2 = 0;
            z3 = 0;
            z4 = 0;
        }

        //implement the getGrid() method
        public void getGrid(){
            System.out.println("To check your Sudoku, enter your board one row at a time, " +
                    "with each digit separated by a space. Hit ENTER at the end of a row.");
            System.out.println("Enter Row 1: ");
                w1 = scan.nextInt();
                w2 = scan.nextInt();
                w3 = scan.nextInt();
                w4 = scan.nextInt();
            System.out.println("Enter Row 2: ");
                x1 = scan.nextInt();
                x2 = scan.nextInt();
                x3 = scan.nextInt();
                x4 = scan.nextInt();
            System.out.println("Enter Row 3: ");
                y1 = scan.nextInt();
                y2 = scan.nextInt();
                y3 = scan.nextInt();
                y4 = scan.nextInt();
            System.out.println("Enter Row 4: ");
                z1 = scan.nextInt();
                z2 = scan.nextInt();
                z3 = scan.nextInt();
                z4 = scan.nextInt();
        }

        //implement the checkGrid() method
        public void checkGrid(){
          //check whether each row adds up to 10 and give feedback
            if (w1 + w2 + w3 + w4 == 10){
                System.out.println("ROW-1: GOOD");
            }
             else {
                System.out.println("ROW-1: BAD");
            }
            if (x1 + x2 + x3 + x4 == 10){
                System.out.println("ROW-2: GOOD");
            }
             else {
                System.out.println("ROW-2: BAD");
            }
            if (y1 + y2 + y3 + y4 == 10){
                System.out.println("ROW-3: GOOD");
            }
             else {
                System.out.println("ROW-3: BAD");
            }
            if (z1 + z2 + z3 + z4 == 10){
                System.out.println("ROW-4: GOOD");
            }
             else {
                System.out.println("ROW-4: BAD");
            }

          //check whether each column adds up to 10 and give feedback
            if (w1 + x1 + y1 + z1 == 10){
                   System.out.println("COL-1: GOOD");
            }
             else {
                System.out.println("COL-1: BAD");
            }
            if (w2 + x2 + y2 + z2 == 10){
                   System.out.println("COL-2: GOOD");
            }
             else {
                System.out.println("COL-2: BAD");
            }
            if (w3 + x3 + y3 + z3 == 10){
                   System.out.println("COL-3: GOOD");
            }
             else {
                System.out.println("COL-3: BAD");
            }
            if (w4 + x4 + y4 + z4 == 10){
                   System.out.println("COL-4: GOOD");
            }
             else {
                System.out.println("COL-4: BAD");
            }

          //check whether each region adds up to 10 and give feedback
            if (w1 + w2 + x1 + x2 == 10){
                   System.out.println("REG-1: GOOD");
            }
             else {
                System.out.println("REG-1: BAD");
            }
            if (y1 + y2 + z1 + z2 == 10){
                   System.out.println("REG-2: GOOD");
            }
             else {
                System.out.println("REG-2: BAD");
            }
            if (w3 + w4 + x3 + x4 == 10){
                   System.out.println("REG-3: GOOD");
            }
             else {
                System.out.println("REG-3: BAD");
            }
            if (y3 + y4 + z3 + z4 == 10){
                   System.out.println("REG-4: GOOD");
            }
             else {
                System.out.println("REG-4: BAD");
            }
        }
}
