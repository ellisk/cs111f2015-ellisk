//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis and Alicia Drosendahl
//CMPSC 111 Fall 2015
//Lab #6
//Date: 30 September 2015 
//
//Purpose: to write a java program that successfully manipulates strings of DNA
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed to read the user's input
import java.util.Random; //needed to create the random mutation

public class dnaString
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kimie Ellis and Alicia Drosendahl\nLab #6 \n" + new Date() + "\n");
		
		Scanner scan = new Scanner(System.in);	
		Random rand = new Random();
		
		//Variable dictionary:
	
		String dnaString;
		String comp1, comp2, comp3, comp4; //represents each step in declaring the complement
		String complement; //complement of dnaString
		int len, locIns, locDel, locRep;
		char insert, randomLet, replaceLet;
		String dnaInsert, dnaDelete, dnaReplace;
		
		

		System.out.println("Please input a string of DNA, consisting only of A, T, C, and G: ");
		dnaString = scan.next(); //declares user input
		dnaString = dnaString.toUpperCase(); //changes dnaString to uppercase
		System.out.println(dnaString);
		
		//print the complement of dnaString
		comp1 = dnaString.replace("A", "B");            //replaces A with a temporary variable to make clear that A being the complement of T is something entirely different than T being the complement of A
		comp1 = comp1.replace("T", "S");                //replaces T with a temporary variable to make a clear distinction between A being the complement of T and T being the complement of A
		comp1 = comp1.replace("C", "F");                //replaces C with a temporary variable to make a clear distinction between G being the complement of C and C being the complement of G
		comp1 = comp1.replace("G", "K");           // replaces G with a temporary variable to make a clear distinction between G being the complement of C and C being the complement of G
	        comp1 = comp1.replace("B","T");            //changes the temporary variable to the complement of A, which is T
	        comp1 = comp1.replace("S","A");            //changes the temporary variable to the complement of T, which is A
	        comp1 = comp1.replace("F","G");            //changes the temporary variable to the complement of C, which is G
	        comp1 = comp1.replace("K","C");            //changes the temporary variable to the complement of G, which is C
		complement = (comp1);
		System.out.println("The complement of " + dnaString + " is " + complement + ".");

		len = dnaString.length(); //finds the length of the string

	//insert a random extra letter into dnaString:
		locIns = rand.nextInt(len + 1); //chooses random integer between 0 and len
		insert = dnaString.charAt(rand.nextInt(len+1)); //picks a character from dnaString at random
		dnaInsert = dnaString.substring(0, locIns) + insert + dnaString.substring(locIns); //puts the random character into the random location
		
		System.out.println("Inserting " + insert + " at position " + locIns + " gives " + dnaInsert + ".");

	//remove a letter from dnaString:
		locDel = rand.nextInt(len+1); //chooses random integer between 0 and len
		dnaDelete = dnaString.substring(0, locDel) + dnaString.substring(locDel+1); //the dnaString without the character in locDel
		System.out.println("Deleting from position " + locDel + " gives " + dnaDelete + "."); 	

	//alter a single letter from a randomly chosen position:
		randomLet = dnaString.charAt(rand.nextInt(len)); //chooses a random letter from dnaString
		locRep = rand.nextInt(len+1); //chooses random integer between 0 and len
		replaceLet = dnaString.charAt(locRep); //defines the character in position locRep that will be replaced
		dnaReplace = dnaString.replace(replaceLet, randomLet); //changes the selected character to a random letter from the original dnaString
		System.out.println("Changing position " + locRep + " gives " + dnaReplace + ".");

	}
}
