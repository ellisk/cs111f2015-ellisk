//=================================================
/* Honor Code: The work I am submitting is a result of my own thinking and efforts.
Kimie Ellis
CMPSC 111 Fall 2015
Lab #4
Date: 16 September 2015

Purpose: Use at least 10 objects of 3 different varieties to create a drawing in Java.
*/
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int WIDTH = 600;
	final int HEIGHT = 400;

	//fill entire background green
	page.setColor(Color.green);
	page.fillRect(0,0, WIDTH, HEIGHT);

	//create a person 
	page.setColor(Color.orange); //head and neck
	page.fillOval(120,60,60,60);
	page.fillRect(142,120,20,20);

	int Shirt_top = 140; //how far down the man's shirt top is located

	page.setColor(Color.red); //shirt
	page.fillRect(115,Shirt_top,70,110);
	page.fillOval(95,Shirt_top,25,25);
	page.fillOval(178,Shirt_top,25,25);
	
	int Pants_top = 250; //how far down the man's pants are located

	page.setColor(Color.blue); //pants
	page.fillRect(118,Pants_top,26,120);
	page.fillRect(156,Pants_top,26,120);

	page.setColor(Color.orange); //arms
	page.fillRect(100,165,10,60);
	page.fillRect(190,165,10,60);

	int Eye_level = 80; //the height of his eyes

	page.setColor(Color.blue); //face
	page.fillOval(140,Eye_level,8,8); //eyes
	page.fillOval(160,Eye_level,8,8);
	page.setColor(Color.red); //mouth
	page.drawArc(140,100,20,10,200,340);
	

	//create a dog
	page.setColor(Color.gray);
	page.fillOval(368,250,40,40);  //head
	page.fillOval(395,265,35,15); //nose
	page.fillOval(250,265,130,80); //body 
	page.fillRect(290,340,10,35); //legs
	page.fillRect(340,340,10,35);
	page.fillOval(240,275,25,25); //tail
	
	page.setColor(Color.black); 
	page.fillOval(395,260,5,5); //eye
	page.drawLine(415,275,410,285); //whiskers
	page.drawLine(425,275,430,285);
	page.drawLine(420,275,420,285);
	page.setColor(Color.magenta); //nose
	page.fillOval(425,266,5,5);

	//insert leash
	page.setColor(Color.magenta);
	page.drawLine(195,225,368,280);



	 
  }
}
