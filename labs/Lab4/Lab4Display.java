//==========================================
/* Honor Code: The work I am submitting is a result of my own thinking and efforts.
Kimie Ellis
CMPSC 111 Fall 2015
Lab #4
Date: 16 September 2015

Purpose: Use at least 10 objects of 3 different varieties to create a drawing in Java.
*/
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Kimie Ellis ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

