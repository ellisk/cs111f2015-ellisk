import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems; //creates an array list from the todoItems in TodoItem
    private static final String TODOFILE = "todo.txt";

    public TodoList() {
        todoItems = new ArrayList<TodoItem>();
    }

    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);
    }

    public Iterator getTodoItems() {
        return todoItems.iterator();
    }

    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        while(fileScanner.hasNext()) {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }

    public void markTaskAsDone(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markDone();
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) {
        //set up the iterator
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>(); //array list of todoItems

        Iterator priorityIterator = todoItems.iterator(); //iterates through TodoItem
        while(priorityIterator.hasNext()){ //while there is another line iterate through the list
            TodoItem todoItem = (TodoItem)priorityIterator.next();
            if (todoItem.getPriority().equals(requestedPriority)){ //if requested Priority is in the todoItem
                priorityList.add(todoItem); //add it to the Priority list
            }
        }
           return priorityList.iterator();  // gavi says this is a sudo println
    }

    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();  //new array

        Iterator categoryIterator = todoItems.iterator();    //iterates through TodoItem
        while(categoryIterator.hasNext()){  //while there is it is --- gavi say
            TodoItem todoItem = (TodoItem)categoryIterator.next();
            if(todoItem.getCategory().equals(requestedCategory)){  //if requested category is in todoItem
                categoryList.add(todoItem);  //add it to category list
            }
        }
         return categoryList.iterator(); //gavi still says this is a sudo println
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
