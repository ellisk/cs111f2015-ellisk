/*
 * Honor Code: The work I am submitting is a result of my own thinking and efforts.
 * Kimie Ellis
 * CMPCS 111 Fall 2015
 * Lab #10
 * 11 November 2015
 *
 * Purpose: Explore iteration constructs and graphical features of Java by conducting a study to evaluate
 *          how loop iterations influence the execution time of a Java program.
*/

import java.awt.Color;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class MandelbrotColor {
    public static void main(String[] args) throws Exception { //main method
        int width = 1920, height = 1080, max = 10000; //initializes the width, height, and maximum (detail)
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); //creates the image
        int black = 0;
        int[] colors = new int[max];
        for (int i = 0; i<max; i++) { //initialize at i=0, condition of i<max, increment to next i
            colors[i] = Color.HSBtoRGB(i/256f, 1, i/(i+8f)); //changes the color distribution
        }

        for (int row = 0; row < height; row++) { //initialize at first row, when row<height, increment to next row
            for (int col = 0; col < width; col++) { //initialize at first column, when col<width, increment to next column
                double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                double r2;
                int iteration = 0;
                while (x*x+y*y < 4 && iteration < max) { //while this condition holds, perform the following
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iteration++;
                }
                if (iteration < max) image.setRGB(col, row, colors[iteration]);
                else image.setRGB(col, row, black);
            }
        }

        ImageIO.write(image, "png", new File("mandelbrot-color.png")); //produces the image in a .png file
    }
}
