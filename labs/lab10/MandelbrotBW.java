/*
 * Honor Code: The work I am submitting is a result of my own thinking and efforts.
 * Kimie Ellis
 * CMPCS 111 Fall 2015
 * Lab #10
 * 11 November 2015
 *
 * Purpose: Explore iteration constructs and graphical features of Java by conducting a study to evaluate how
 *          loop iterations influence the execution time of a Java program.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class MandelbrotBW {
    public static void main(String[] args) throws Exception { //main method
        int width = 1920, height = 1080, max = 1000; //initializes width, height, and maximum
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); //creates the image
        int black = 0x000000, white = 0xFFFFFF; //establishes that the image is in black and white

        for (int row = 0; row < height; row++) { //initializes first row, when row<height, increments to next row
            for (int col = 0; col < width; col++) { //initializes first column, when column<width, increments to next column
                double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                int iterations = 0;
                while (x*x+y*y < 4 && iterations < max) { //execute the body while this statement holds true
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iterations++;
                }
                if (iterations < max) image.setRGB(col, row, white);
                else image.setRGB(col, row, black);
            }
        }
        ImageIO.write(image, "png", new File("mandelbrot-bw.png")); //produces the image in a .png file
    }
}
