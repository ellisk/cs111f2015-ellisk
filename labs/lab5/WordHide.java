//****************************************
// Honor Code:  The work I am submitting is a result of my own thinking and efforts.
// Jordan Caldwell and Kimie Ellis
// CMPSC 111 Fall 2015
// Lab #5
// Date: September 23, 2015
//
// Purpose: To explore the ideas of a "class" and an "object" in the Java programming language.
//	    To inspect and manipulate a String object.
//	    To work on a real-world programming task.
//****************************************
import java.util.Date;
import java.util.Scanner;
import java.lang.String;
//import java.util.Random;         // possible addition in future

public class WordHide
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Jordan Caldwell and Kimie Ellis\nLab #5\n" + new Date() + "\n");

		Scanner scan = new Scanner(System.in);
//		Random generator = new Random();            // possible addition in future

		// Variable dictionary:

		String word, uppercase;
		String substr;
		char one, two, three, four, five, six, seven, eight, nine, ten;  //each character from substr
		
		System.out.println("Please enter a ten character word:");                           // input the string
//		System.out.println("(if less than ten, add * after for a string length of ten)");
		System.out.println("(if less than ten, add asdfghjkl to the end,\nas needed, or don't add anything at all)");
		word = scan.nextLine();
//		substr = word.replace('*', ' ');					            // manipulate the string
		substr = word.concat("raspberry");        // to add characters to a short string 
		substr = substr.substring(0,10);

//		System.out.println(word);                   // for testing
		uppercase = substr.toUpperCase();
//		System.out.println(uppercase);              // for testing

		one = uppercase.charAt(0);		    // Get each character from the string
		two = uppercase.charAt(1);
		three = uppercase.charAt(2);
		four = uppercase.charAt(3);
		five = uppercase.charAt(4);
		six = uppercase.charAt(5);
		seven = uppercase.charAt(6);
		eight = uppercase.charAt(7);
		nine = uppercase.charAt(8);
		ten = uppercase.charAt(9);

//		System.out.println(one);       // for testing

		/* Words to test with
		   pineapples
		   thirtyfour
		   applesauce
		   razzmatazz
		   oxidizable
		   circumflex        */


		/* When adding in the print statements, add a "/n" to the end of each
		   so that the next printf() starts on the next line                  */
		
		System.out.println(" ");
		System.out.println("----------");             // begin the grid of 20x20 letters with the string hidden inside

		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", three, two, seven, eight, "X", five, "Z", four, one, six, nine, ten, ten, "E", "N", "S", "T", "N", "P", "E\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", two, one, "Z", "W", seven, "E", nine, one, "E", ten, "I", "M", five, three, six, "U", "D", eight, four, "Q\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "E", "T", five, seven, "O", nine, two, "P", "S", "I", three, eight, ten, ten, "E", "R", two, seven, "E", "R\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "S", "R", "E", four, six, ten, "Q", "U", "K", "T", one, nine, ten, three, six, "E", "R", "T", "N", "A\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "B", "A", "V", six, four, eight, ten, "M", "S", three, "W", seven, "Y", "O", "P", two, "T", "L", "A", "R\n");


		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", nine, three, "I", "F", "A", four, seven, eight, "E", "A", "P", six, "C", "R", ten, one, "E", two, "T", "U\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "H", "G", five, eight, "D", "E", "T", nine, three, one, "R", "T", ten, "L", two, seven, "O", "M", one, "I\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", nine, "I", seven, "P", "A", four, eight, "I", "C", "V", "O", five, ten, three, "S", "P", six, eight, "U", "D\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", ten, seven, "O", "R", three, "W", "S", "U", one, five, "N", "F", four, "M", eight, two, nine, three, "G", "A\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "A", "R", ten, ten, "S", five, "T", seven, "L", "G", "U", five, "N", one, "E", "H", "Y", three, "P", "D\n");


		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "R", "D", four, "A", eight, two, "M", "I", "O", nine, six, "C", "F", four, "G", "E", "N", ten, one, "V\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", two, "K", five, "L", "H", "T", eight, "S", six, seven, "J", "D", four, "X", "U", "E", nine, five, "L", "R\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", seven, "I", six, "F", "C", "U", ten, "Y", eight, "B", three, two, one, "O", "S", "N", eight, "W", "A", "S\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "J", "O", "T", four, "N", "A", "H", nine, nine, "I", "M", "U", five, ten, "P", nine, "D", two, "G", "F\n"); 
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s",one , six, "R", "E", "Z", "L", ten, "A", "K", "O", "B", "D", three, four, five, "S", six, "Y", "V", "I\n"); 


		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", seven, "T", "P", eight, one, "E", "F", two, nine, "A", ten, three, "U", six, "Y", four, "O", "H", five, "D\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "M", "W", three, "S", five, nine, "Q", one, "U", "F", six, ten, "I", two, "C", seven, "V", four, "B", "K\n"); 
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "Z", "R", eight, three, "P", "R", one, six, ten, "S", "T", "F", two, seven, "O", one, "E", five, "S", "J\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", five, "E", ten, "R", "F", two, eight, "A", "P", one, nine, "S", "D", "B", seven, three, "P", "H", four, "G\n");
		System.out.printf("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", six, four, "T", "C", seven, "M", one, three, "U", "A", five, "B", "W", "D", eight, "X", "R", "L", two, "K\n");
		
		System.out.println("----------");
		System.out.println(" ");

		/* The positions for the string should be:
		   first letter should be at (6,16)
		   last letter should be at (16,6) */		

		System.out.println("Have a nice day!\n");

		// end of program
		
	}
}
