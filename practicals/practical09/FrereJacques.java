import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner; //implement scanner

import org.jfugue.Note;
import org.jfugue.Pattern;
import org.jfugue.PatternTransformer;
import org.jfugue.Player;
import org.jfugue.extras.ReversePatternTransformer;

public class FrereJacques
{
    public static void main(String[] args)
    {
        int repeats; //declaring repeat variable
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter the number of times the song should repeat:");
        repeats = scan.nextInt(); //takes the user input for number of repeats

        // "Frere Jacques"
        String[] pattern1Array = {"C5q", "D5q", "E5q", "C5q"};
        StringBuilder builder = new StringBuilder();
        for(String partialPattern : pattern1Array) {
            builder.append(partialPattern);
            builder.append(" ");
        }
        Pattern pattern1 = new Pattern(builder.toString());

        // "Dormez-vous?"
        Pattern pattern2 = new Pattern("E5q F5q G5h");

        // "Sonnez les matines"
        Pattern pattern3 = new Pattern("G5i A5i G5i F5i E5q C5q");

        // "Ding ding dong"
        Pattern pattern4 = new Pattern("C5q G4q C5h");

        // Put all of the patterns together to form the song
        Pattern song = new Pattern();
        for(int i=0; i < repeats; i++){ //initialize at 0, when i < repeats, increment one at a time
        song.add(pattern1, 2); // Adds 'pattern1' to 'song' twice
        song.add(pattern2, 2); // Adds 'pattern2' to 'song' twice
        song.add(pattern3, 2); // Adds 'pattern3' to 'song' twice
        song.add(pattern4, 2); // Adds 'pattern4' to 'song' twice
        }

        // Play the song!
        Player player = new Player(); player.play(song);

        try {
            player.saveMidi(song, new File("frerejacues.mid"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.close();
    }

}






