//=================================================
// Draw two boxes that display a color and the complement of the color.
//=================================================

import java.awt.*;
import javax.swing.JApplet;

public class PaintDrawingCanvas extends JApplet {

  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page) {
    // create the color based on the input values from the user
    Color userColor = new Color(DisplayDrawingCanvas.redValue,
        DisplayDrawingCanvas.greenValue, DisplayDrawingCanvas.blueValue);

    // fill the first half (left-to-right) with the user's color
    // make a call to page.fillRect with the correct parameters
    page.setColor(userColor);
	page.fillRect(0,0,300,400);

    // calculate the "complementary" color of the provided color
    // and then create a new Color object called userComplementaryColor.
    // Refer to notes in the practical assignment sheet about this calculation
    // (add your own calculation to replace the value of null)
    Color userComplementaryColor = new Color(255-DisplayDrawingCanvas.redValue, 255-DisplayDrawingCanvas.greenValue, 255-DisplayDrawingCanvas.blueValue);

    // fill the second half (left-to-right) with the complement of the user's color
    // make a call to page.fillRect with the correct parameters
    page.setColor(userComplementaryColor);
	page.fillRect(300,0,600,400);

  }
}
