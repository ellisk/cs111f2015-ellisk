//====================================
// CMPSC 111
// Practical 6
// 16 October 2015
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6 {

    public static void main(String[] args) {

        System.out.println("Gregory M. Kapfhammer\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky, octo;           // declares two different octopi
        Utensil spat, spoon;          // two different kitchen utensils

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky");    // create and name the octopus
        ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        System.out.println("Using 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // automatically produce a visualization of the octopus
        LJV.Context dataVisualizationContext = LJV.getDefaultContext();
        dataVisualizationContext.outputFormat = "pdf";
        dataVisualizationContext.ignorePrivateFields = false;
        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-before.pdf" );

        // Use methods to change some values:
        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nUsing 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-after.pdf" );

//********************************************************************************************************
    //the second octopus:

        spoon = new Utensil("spoon"); //create a spoon
        spoon.setColor("red");        //set spoon color
        spoon.setCost(3.50);          //set spoon price


        /*I am still having some difficulty getting this code to compile. I have scheduled office hours
         * with you on Monday in order to help me resolve this issue. */
        Octopus octo = new Octopus ("Octo", age, weight, utensil)
        {
        age = 3;                            //set octo's age
        weight = 80;                        //set octo's weight
        utensil = spoon;                  //assign the spoon to octo
        };
     // Octopus octo = new Octopus("Octo", a, w, utensil); //create and name a new octopus


        //print out the information about octo and his utensil:
        System.out.println("Using 'get' methods:");
        System.out.println(octo.getName() + " weighs " +octo.getWeight()
            + " pounds\n" + "and is " + octo.getAge()
            + " years old. His favorite utensil is a "
            + octo.getUtensil());

        System.out.println(octo.getName() + "'s " + octo.getUtensil() + " costs $"
            + octo.getUtensil().getCost());
        System.out.println("Utensil's color: " + spoon.getColor());

        LJV.drawGraph( dataVisualizationContext, octo, "my-ocky.pdf" );



    }
}
