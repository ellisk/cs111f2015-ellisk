import java.lang.Math;

public class Kinetic
{
//the "int" command introduces variables that you can reference later
    public static String computeVelocity(int kinetic, int mass)
   {
        int velocity_squared = 0;
        int velocity = 0;
        StringBuffer final_velocity = new StringBuffer();

//the following two lines are put into comment form so that a future user of the code knows what the original writer was trying to do
        //System.out.println("F V: " + velocity);
        //System.out.println("F V squared: " + velocity_squared);

//the following lines are setting up the formula that we would like the program to calculate
        if( mass != 0 ) {
            velocity_squared = 2 * (kinetic / mass);
            velocity = (int)Math.sqrt(velocity_squared);
            final_velocity.append(velocity);
        }

        else {
            final_velocity.append("Undefined");
        }

        //System.out.println("L V: " + velocity);
        //System.out.println("L V squared: " + velocity_squared);

        return final_velocity.toString();
    }
/*the following lines are what the code is going to print out
  the words inside the quotation marks will appear in the print out
  for every part you wish to add to the line,  you must use + to have all parts appear*/
    public static void main(String[] args) {
        System.out.println("Calling the computeVelocity method!");
        String velocity = computeVelocity(1000,5);
        System.out.println("The velocity is: " + velocity);
    }
}
