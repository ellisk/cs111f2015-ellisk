/* ***************************************
Honor Code: The work I am submitting is a result of my own thinking and efforts.
Kimie Ellis
CMPSC 111 Fall 2015
Practical #2
Date: 11 September 2015

Purpose:
****************************************** */

import java.util.Date; //needed for printing today's date

public class Practical02
{
	//----------------------------------
	//main method: program execution begins here
	//----------------------------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Kimie Ellis\nPractical #2\n" + new Date() + "\n");

		//Variable dictionary

		System.out.println(" ____________");
		System.out.println("|  _      _  \\");
		System.out.println("| |_|  | |_|  \\___");
		System.out.println("|     *|*         |");
		System.out.println("---(@)-----(@)-----");
		
	}
}
