//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Practical #7
//Date: 23 October 2015
//
//Purpose: Create a guessing game that tells the user whether their guess is too
//          high or too low until they guess the correct number.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //import the scanner
import java.util.Random; //imports the random number generator

public class Practical7
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
        System.out.println("Kimie Ellis\nPractical #7 \n" + new Date() + "\n");

		//Variable dictionary:

        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        int num, guess;  //declare number and guess variables
        int numTries = 0;  //initializes the number of tries to guess the correct number
        num = rand.nextInt(101);  //randomly choose a variable between 0-100

        System.out.println("Please input a guess of an integer 0-100");
        guess = scan.nextInt();

        while (num != guess){
            if (num > guess){
                System.out.println("Too low!");
                guess = scan.nextInt();
            }
            if (num < guess){
                System.out.println("Too high!");
                guess = scan.nextInt();
            }
        numTries++;
        }
        System.out.println("Congratulations! After guessing " + numTries + " times, " +
                num + " is the correct number.");

    /*    if (num == guess){
             System.out.println("Congratulations! After guessing " + numTries + " times, " +
                     num + " is the correct number.");
        } */


	}
}

