//*****************************
//Kimie Ellis
//CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

//I am still working on completing this practical, but have no time this Friday. I will attend to it this weekend.

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear()
    {
        //use modular arithmetic instead of division
        //put print statements into the main method, only use true/false outcomes here
        if(year/100){
            if(year/400){
                System.out.println(y + " is a leap year!");
            }
             else{
                 System.out.println(y + "is not a leap year.");
             }
        }else if(y/4){
                 System.out.println(y + "is  a leap year!");
                }
         else{System.out.println(y + "is not a leap year.");
        }

        //need return statement

    // a method that checks if the user's input year is a cicada year

    public boolean isCicadaYear(int n)
    {
        if(y==2013+17*n){
            System.out.println("It's an emergence year for Brood II of the 17-year cicadas.");
        }
        else{System.out.println("It is not an emergence year for Brood II of the 17-year cicadas.");}
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear(int n)
    {
        if(y==2013+11*n){
             System.out.println("It's a peak sunspot year.");
        }
         else{System.out.println("It's not a peak sunspot year.");}
    }
}
