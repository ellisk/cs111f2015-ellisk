//****************************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kimie Ellis
//CMPSC 111 Fall 2015
//Practical #4
//Date: 2 October 2015
//
//Purpose: Create a Mad Lib that prints out a story that includes at least one calculation.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; 

public class MadLib
{
	//-------------------------------
	//main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kimie Ellis\nPractical #4 \n" + new Date() + "\n");

		Scanner scan = new Scanner(System.in); //declares the scanner variable

		//Variable dictionary:
		
		String name, prof1;
		int hour1, num2, hour2;
		String adj1, adj2, adj3;
		String noun1, noun2;
		String ingverb1, ptverb, advrb;
		String anml1, bldng, clth;
	//	int num2;

		//input code			
		System.out.println("Enter the name of someone in the class: ");
		name = scan.nextLine();
		System.out.println("Enter a number between 1 and 12: ");
		hour1 = scan.nextInt();

		System.out.println("Enter an adjective: ");
		adj1 = scan.nextLine();
		adj1 = scan.nextLine();
		System.out.println("Enter a noun: ");
		noun1 = scan.nextLine();
		System.out.println("Enter a verb ending in \"ing\":");
		ingverb1 = scan.nextLine();
		System.out.println("Enter an animal: ");
		anml1 = scan.nextLine();

		System.out.println("Enter another adjective: ");
		adj2 = scan.nextLine();
		System.out.println("Enter the name of a professor: ");
		prof1 = scan.nextLine();

		System.out.println("Enter another noun: ");
		noun2 = scan.nextLine();
		System.out.println("Enter a past tense verb: ");
		ptverb = scan.nextLine();
		System.out.println("Enter the name of a building on campus: ");
		bldng = scan.nextLine();

		System.out.println("Enter an article of clothing: ");
		clth = scan.nextLine();

		System.out.println("Enter an adverb: ");
		advrb = scan.nextLine();

		System.out.println("Enter number between 1 and 24: ");
		num2 = scan.nextInt();
		System.out.println("Enter another adjective: ");
		adj3 = scan.nextLine();
		adj3 = scan.nextLine();

		hour2 = (hour1 + num2) % 12;		


		//output story
		
		System.out.println("When " + name + " woke up at " + hour1 + ":00, he/she was very confused about what was going on. ");
		System.out.println(name + " had just had a nightmare in which a " + adj1 + " " + noun1 + " was " + ingverb1 + " with a wild " + anml1 + ".");
		System.out.println("The " + adj2 + " " + anml1 + " soon morphed into Professor " + prof1 + "!");
		System.out.println("Professor " + prof1 + " kept asking about an overdue " + noun2 + ", until " + name + " finally " + ptverb + " away to the safety of " + bldng + ".");
		System.out.println("The nightmare got even worse when it turned out, " + name + " was now in the front of the classroom wearing only a " + clth + "!");
		System.out.println(advrb + ", " + name + " woke up right after that.");
		System.out.println("The worst part about it, is that " + name + " knew that in " + num2 + " hours, he/she would fall asleep again, and who knew what kind of " + adj3 + " nightmare would happen at " + hour2 + ":00...");

	}
}
